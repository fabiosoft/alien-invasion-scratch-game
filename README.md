# Invasione Aliena

**Space Invaders** versione per [_Scratch_](https://scratch.mit.edu/).  
Completa tutti i livelli per vincere!

-> e <- movimenti

↑ spara

### Caratteristiche:
- Livelli di gioco multipli
- Navicelle nemiche in movimento
- Le navicelle verdi sono più resistenti